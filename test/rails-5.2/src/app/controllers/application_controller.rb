class ApplicationController < ActionController::API
  def index
    # validate asset precompile
    if !Rails.root.join('public', 'assets', 'application-e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855.css').exist?
      render body: 'Assets were not precompiled'
      return
    end

    render body: 'Hello World'
  end
end
