# duke-ruby

This is a Community Supported Resource that is t is offered as part of the
[Duke Openshift Users Group Community Supported Resources](https://duke_openshift_users.pages.oit.duke.edu/community-supported-resources/guide).


This project contains  a base docker image that is intended to be used with s2i or as a
parent image in a custom Dockerfile.

duke-ruby is based off of https://hub.docker.com/_/ruby   In addition to the upstream
code, it also includes scripts to support s2i, commonly needed packages, and is regularly
patched.

## Tags

The following tags are used with this image.

As of July 6, 2019, ```buster``` is the stable debian release and ```stretch``` is the oldstable debian release.

| tag | |
| --- | --- |
| ```:2.7-buster``` | Ruby 2.7 (based on 2.7-buster tag from https://hub.docker.com/_/ruby) |
| ```:2.6-stretch``` | Ruby 2.6 (based on 2.6-stretch tag from https://hub.docker.com/_/ruby) |
| ```:2.6-buster``` | Ruby 2.6 (based on 2.6-buster tag from https://hub.docker.com/_/ruby) |
| ```:2.6-alpine3.9``` | Ruby 2.6 (based on 2.6-alpine3.9 tag from https://hub.docker.com/_/ruby) |
| ```:2.5-stretch``` | Ruby 2.5 (based on 2.5-stretch tag from https://hub.docker.com/_/ruby) |

## Use with a Dockerfile

duke-ruby is based off of https://hub.docker.com/_/ruby.  If you wish to use it with a
custom Dockerfile, its recommended to follow their instructions.

## Use with s2i
(For more documentation on s2i, see https://github.com/openshift/source-to-image)

To build ruby code with s2i, you can run
```s2i build https://gitlab.oit.duke.edu/mygroup/myapp duke-ruby:2.7 myapp-image-name```

The s2i scripts will automatically run ```bundle install``` (if necessary) when building your
image as well as running ```rake assets:precompile``` (if necessary).

By default, your image will be run with ```bundle exec rackup``` with your application
listening on port 8080.

### Custom build steps

If you need custom build steps in your application, you can create a script called
```.s2i/assemble```.  That file will be run as part of the build, allowing you to run
any necessary steps.

### Custom start script

If you need a custom startup with s2i, you can supply a ```.s2i/run``` script in your
project.  If that script exists, the container will start with ```bundle exec .s2i/run```
instead of calling rackup.

## Examples

For examples, please see the [test](test) directory.

## Test Layout

Each test directory includes a couple files to tell the test harness how to function. In
addition, each test directory as a ```src/``` directory that contains the application
source code that will be used for the build.

The supported test harness files are:

| filename | |
| --- | --- |
| expected-httpout | If this file exists, its contents must perfectly match the body from returned from ```GET /``` in the test application |
| expected-stdout | If this file exists, its contents must perfectly match the ```docker logs``` output from the test container |
| expected-stdout-partial | If this file exists, the output of ```docker logs``` must contain the contents of this file |
| run-in-background | If this file exists, the container will be run in the background while other checks are run.  Otherwise, the container runs to completion before checks are run |
| src/Dockerfile | If a Dockerfile exists in the source tree, a docker build will be used to build the test image instead of s2i |
