FROM docker.io/ruby:2.6-buster
MAINTAINER Sean Dilda <sean@duke.edu>

ARG CI_COMMIT_SHA=unspecified
LABEL git_commit=${CI_COMMIT_SHA}

ARG CI_PROJECT_URL=unspecified
LABEL git_repository_url=${CI_PROJECT_URL}

LABEL io.openshift.s2i.scripts-url=image:///usr/libexec/s2i
LABEL name 'duke-ruby'
LABEL version '2.6-buster'
LABEL release '1'
LABEL architecture 'x86_64'
LABEL distribution-scope 'public'
LABEL authoritative-source-url 'https://gitlab.oit.duke.edu/community-supported-resources/duke-ruby'

ARG CI_COMMIT_SHA=unspecified
LABEL git_commit=${CI_COMMIT_SHA}

ARG CI_PROJECT_URL=unspecified
LABEL git_repository_url=${CI_PROJECT_URL}

WORKDIR /usr/src/app

# Setup generic home directory since OKD will give us a random UID
RUN mkdir -p /home/appuser && \
    chgrp 0 /home/appuser && \
    chmod g+w /home/appuser
ENV HOME=/home/appuser

# Apply any pending patches and install some basic dependencies
RUN apt-get update -qq \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends dumb-init yarn postgresql-client mariadb-client \
    && rm -rf /var/lib/apt/lists/*

# Copy in s2i scripts
COPY ./s2i/bin /usr/libexec/s2i

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
